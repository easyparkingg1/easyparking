from django.contrib import admin

from principal.models import Registro, Tipo_vehiculo, Usuario

# Register your models here.
admin.site.register(Registro)
admin.site.register(Usuario)
admin.site.register(Tipo_vehiculo)



from django.db import models
from django.db.models import fields
from django.forms import ModelForm, widgets

# Create your models here.
class RegistroVehiculos (models.Model):
    idServicio = models.AutoField(primary_key=True, null=False)
    matriculaVehiculo = models.CharField(max_length=6, null=False)

    listaVehiculos = [('M', 'Moto'), ('A', 'Automóvil')]
    tipoVehiculo = models.CharField(max_length = 8, null=False, choices=listaVehiculos)

    ingreso = models.DateTimeField(null=False)
    salida = models.DateTimeField(null=True, blank=True)
   

    tiempoEstadia = models.TimeField(null=True, blank=True)
    costoServicio = models.IntegerField(null=True, blank=True)
 

class RegistroForm (ModelForm):
    class Meta:
        model = RegistroVehiculos
        fields=['matriculaVehiculo', 'tipoVehiculo',]
        widgets={'matriculaVehiculo': widgets.TextInput(attrs={'class':'form-control'}),
        'tipoVehiculo': widgets.Select(attrs={'class':'form-control'})}
                

        #Id servicio se necesitaria?
        # 'tiempoEstadia': widgets.TimeField(attrs={'class':'form-control'}),
        #'costoServicio': widgets.TextInput(attrs={'class':'form-control'})





# Generated by Django 3.2.7 on 2021-10-07 16:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registro', '0003_alter_registrovehiculos_tipovehiculo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrovehiculos',
            name='costoServicio',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='registrovehiculos',
            name='salida',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='registrovehiculos',
            name='tiempoEstadia',
            field=models.TimeField(null=True),
        ),
    ]

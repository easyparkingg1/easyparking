from django.shortcuts import render

from .models import RegistroForm, RegistroVehiculos
from datetime import datetime, timezone

# Create your views here.
def home (request):
    if request.method == 'GET':
        return render(request, 'home.html', {'formulario':RegistroForm()})
    else:
        form = RegistroForm(request.POST)
        fechaActual = datetime.now(timezone.utc)
        
        if form.is_valid():
            a=RegistroVehiculos(matriculaVehiculo=form.cleaned_data['matriculaVehiculo'],
            tipoVehiculo=form.cleaned_data['tipoVehiculo'])
            a.ingreso=fechaActual
            
            a.save()
        return render(request, 'home.html', {'formulario':RegistroForm()})


def registro(request):
    reg = RegistroVehiculos.objects.all()
    return render(request, 'registros.html', {'reg':reg})

def salida(request, id):
    fechaActual = datetime.now(timezone.utc)
    reg = RegistroVehiculos.objects.get(pk = id)
    tiempo = fechaActual - reg.ingreso
    reg.salida = fechaActual
    #reg.tiempoEstadia = tiempo
    print(tiempo)
    seconds = tiempo.seconds
    minutes = (tiempo.seconds//60)%60
    hours = seconds//3600
    reg.tiempoEstadia = str(hours) + ":" + str(minutes) + ":00"
    if reg.tipoVehiculo == "A":  
        reg.costoServicio = minutes * 90
    else:
        reg.costoServicio = minutes * 50
    reg.save()
    return render(request, 'home.html')